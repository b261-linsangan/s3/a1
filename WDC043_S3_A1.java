package com.zuitt.example;

import java.util.Scanner;

public class WDC043_S3_A1 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int whileLoop_answer = 1;
        int forLoop_answer = 1;
        int counterWhile = 1;
        int counterFor = 1;

        System.out.println("Input an integer whose factorial will be computed: ");

        //while loop
        int num = in.nextInt();


        //input 5 -> expected 120
        if (num != 0 && num > 0) {

            //while loop
            while (counterWhile <= num) {
                whileLoop_answer *= counterWhile;
                counterWhile++;
            }
            System.out.println("The factorial of " + num + " is " + whileLoop_answer + " using while loop");

            //for loop
            for (int i = counterFor; i <= num; i++) {
                forLoop_answer *= i;
            }
            System.out.println("The factorial of " + num + " is " + forLoop_answer + " using for loop");

        } else {
            System.out.println("Invalid input");
        }







    }

}
