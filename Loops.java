package com.zuitt.example;

import java.lang.reflect.Array;
import java.net.Inet4Address;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Loops {

    public static void main(String[] args) {

        //while loop
//        int x = 0;
//        while (x < 10) {
//            System.out.println("Nice: " + x);
//            x++;
//        }

        //do-while loop
//        int y = 10;
//        do {
//            System.out.println("Countdown" + y );
//            y--;
//        }  while(y > 10);
//
        String[][] classroom = new String[3][3];

        classroom[0][0] = "Athos0";
        classroom[0][1] = "Porthos0";
        classroom[0][2] = "Warr0";

        classroom[1][0] = "Athos1";
        classroom[1][1] = "Porthos1";
        classroom[1][2] = "Warr1";

        classroom[2][0] = "Athos2";
        classroom[2][1] = "Porthos2";
        classroom[2][2] = "Warr2";

//        for(int row = 0; row < 3; row++) {
//            for(int col = 0; col < 3; col++){
//                System.out.println(classroom[row][col]);
//            }
//        }

        System.out.println(Arrays.deepToString(classroom));

//        for(String[] row: classroom) {
//            for(String column : row) {
//                System.out.println(column);
//            }
//        }

//       Syntax:

        ArrayList<Integer> numbers = new ArrayList<>();

//        numbers.add(5);
//        numbers.add(10);
//        numbers.add(15);
//        numbers.add(20);
//        numbers.add(25);
//        System.out.println("ArrayList " + numbers);

        // ' -> ' this is lamda operator which is used to separate parameter and implemetation
        numbers.forEach(number -> System.out.println(number));

        //forEach with HashMaps
        //Syntax:
        // HashmapName.forEach((key, value) -> codeblock);

        HashMap<String, Integer> grades = new HashMap<>(){{
            put("English", 90);
            put("Math", 95);
            put("Science", 97);
            put("History", 94);
        }};

        grades.forEach((subject, grade) -> System.out.println(subject + ": " + grade));



    }


}
