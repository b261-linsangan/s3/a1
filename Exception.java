package com.zuitt.example;

import java.util.Scanner;

public class Exception {
    public static void main(String[] args) {

        //exceptions
        //a problem that arises during the execution of a program
        //it disrupts the normal flow of the program and terminates i
        //abnormally

        //Exception Handling
        //refers to managing and catching run-time errors in order
        //to safely run our code

        Scanner input = new Scanner(System.in);
        int num = 0;

        System.out.println("Please enter a number: ");
//
//        try to do execute the statement
        try {
            num = input.nextInt();
        }
        catch (java.lang.Exception e) {
            System.out.println("Invalid input");
            //will print the error
            e.printStackTrace();
        }
        finally {
            System.out.println("You have entered: " + num);
        }

//        num = input.nextInt();
//        System.out.println("You have entered: " + num);

        //errors encountered in java
            //compile-time - during compile
            //run-time


    }
}
